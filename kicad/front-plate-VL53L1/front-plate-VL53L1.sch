EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title "Millibot - sprednja ploščica"
Date "2021-04-09"
Rev ""
Comp "Fakulteta za strojništvo, Univerza v Ljubljani"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J1
U 1 1 6070B223
P 4550 2550
F 0 "J1" H 4600 2967 50  0000 C CNN
F 1 "To top plate" H 4600 2876 50  0000 C CNN
F 2 "Molex_SlimStack:SlimStack_0.8mm_Male_10" H 4550 2550 50  0001 C CNN
F 3 "~" H 4550 2550 50  0001 C CNN
	1    4550 2550
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J2
U 1 1 6070D476
P 4550 3950
F 0 "J2" H 4600 4367 50  0000 C CNN
F 1 "To bottom plate" H 4600 4276 50  0000 C CNN
F 2 "Molex_SlimStack:SlimStack_0.8mm_Male_10" H 4550 3950 50  0001 C CNN
F 3 "~" H 4550 3950 50  0001 C CNN
	1    4550 3950
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR02
U 1 1 60872A64
P 6500 6750
F 0 "#PWR02" H 6500 6600 50  0001 C CNN
F 1 "+3V3" H 6515 6923 50  0000 C CNN
F 2 "" H 6500 6750 50  0001 C CNN
F 3 "" H 6500 6750 50  0001 C CNN
	1    6500 6750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 6087025F
P 6500 8350
F 0 "#PWR01" H 6500 8100 50  0001 C CNN
F 1 "GND" H 6505 8177 50  0000 C CNN
F 2 "" H 6500 8350 50  0001 C CNN
F 3 "" H 6500 8350 50  0001 C CNN
	1    6500 8350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 6086DCE2
P 7000 7550
F 0 "C2" H 7115 7596 50  0000 L CNN
F 1 "4.7uF" H 7115 7505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7038 7400 50  0001 C CNN
F 3 "~" H 7000 7550 50  0001 C CNN
	1    7000 7550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 6086CE3A
P 6500 7550
F 0 "C1" H 6615 7596 50  0000 L CNN
F 1 "100nF" H 6615 7505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6538 7400 50  0001 C CNN
F 3 "~" H 6500 7550 50  0001 C CNN
	1    6500 7550
	1    0    0    -1  
$EndComp
$Comp
L Sensor_Distance:VL53L1CXV0FY1 U1
U 1 1 60C483F3
P 5150 7550
F 0 "U1" H 5480 7596 50  0000 L CNN
F 1 "VL53L1CXV0FY1" H 5480 7505 50  0000 L CNN
F 2 "Sensor_Distance:ST_VL53L1x" H 5825 7000 50  0001 C CNN
F 3 "https://www.st.com/resource/en/datasheet/vl53l1x.pdf" H 5250 7550 50  0001 C CNN
	1    5150 7550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 8150 5150 8250
Wire Wire Line
	5150 8250 5250 8250
Wire Wire Line
	5250 8250 5250 8150
Wire Wire Line
	5250 8250 6500 8250
Wire Wire Line
	6500 8250 6500 7700
Connection ~ 5250 8250
Wire Wire Line
	7000 7700 7000 8250
Wire Wire Line
	7000 8250 6500 8250
Connection ~ 6500 8250
Wire Wire Line
	5150 6950 5150 6850
Wire Wire Line
	5150 6850 5250 6850
Wire Wire Line
	5250 6850 5250 6950
Wire Wire Line
	5250 6850 6500 6850
Wire Wire Line
	6500 6850 6500 7400
Connection ~ 5250 6850
Wire Wire Line
	6500 6850 7000 6850
Wire Wire Line
	7000 6850 7000 7400
Connection ~ 6500 6850
Wire Wire Line
	6500 8350 6500 8250
Wire Wire Line
	6500 6750 6500 6850
Text GLabel 4250 7550 0    50   Input ~ 0
I2C_SDA
Text GLabel 4250 7650 0    50   Input ~ 0
I2C_CLK
Text GLabel 4250 7850 0    50   Input ~ 0
XSHUT
Wire Wire Line
	4250 7550 4750 7550
Wire Wire Line
	4250 7650 4750 7650
Wire Wire Line
	4250 7850 4750 7850
$Comp
L power:GND #PWR0101
U 1 1 60C4BC51
P 3800 2750
F 0 "#PWR0101" H 3800 2500 50  0001 C CNN
F 1 "GND" H 3805 2577 50  0000 C CNN
F 2 "" H 3800 2750 50  0001 C CNN
F 3 "" H 3800 2750 50  0001 C CNN
	1    3800 2750
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0102
U 1 1 60C4DB8E
P 5400 2750
F 0 "#PWR0102" H 5400 2600 50  0001 C CNN
F 1 "+3V3" H 5415 2923 50  0000 C CNN
F 2 "" H 5400 2750 50  0001 C CNN
F 3 "" H 5400 2750 50  0001 C CNN
	1    5400 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 2750 4250 2750
Wire Wire Line
	4850 2750 5000 2750
Wire Wire Line
	4850 2650 5000 2650
Wire Wire Line
	5000 2650 5000 2750
Connection ~ 5000 2750
Wire Wire Line
	5000 2750 5400 2750
Wire Wire Line
	4350 2650 4250 2650
Wire Wire Line
	4250 2650 4250 2750
Connection ~ 4250 2750
Wire Wire Line
	4250 2750 4350 2750
Text GLabel 4000 4050 0    50   Input ~ 0
I2C_CLK
Wire Wire Line
	4350 4050 4000 4050
Text GLabel 4000 4150 0    50   Input ~ 0
I2C_SDA
Wire Wire Line
	4350 4150 4000 4150
Text GLabel 4000 3950 0    50   Input ~ 0
XSHUT
Wire Wire Line
	4350 3950 4000 3950
$Comp
L power:GND #PWR?
U 1 1 60C50FFA
P 3300 3750
F 0 "#PWR?" H 3300 3500 50  0001 C CNN
F 1 "GND" H 3305 3577 50  0000 C CNN
F 2 "" H 3300 3750 50  0001 C CNN
F 3 "" H 3300 3750 50  0001 C CNN
	1    3300 3750
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 60C517A1
P 5450 3750
F 0 "#PWR?" H 5450 3600 50  0001 C CNN
F 1 "+3V3" H 5465 3923 50  0000 C CNN
F 2 "" H 5450 3750 50  0001 C CNN
F 3 "" H 5450 3750 50  0001 C CNN
	1    5450 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 3750 4950 3750
Wire Wire Line
	4850 3850 4950 3850
Wire Wire Line
	4950 3850 4950 3750
Connection ~ 4950 3750
Wire Wire Line
	4950 3750 5450 3750
Wire Wire Line
	3300 3750 4250 3750
Wire Wire Line
	4350 3850 4250 3850
Wire Wire Line
	4250 3850 4250 3750
Connection ~ 4250 3750
Wire Wire Line
	4250 3750 4350 3750
$EndSCHEMATC
