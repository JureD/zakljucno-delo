EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title "Millibot - sprednja ploščica"
Date "2021-04-09"
Rev ""
Comp "Fakulteta za strojništvo, Univerza v Ljubljani"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J1
U 1 1 6070B223
P 4550 2550
F 0 "J1" H 4600 2967 50  0000 C CNN
F 1 "To top plate" H 4600 2876 50  0000 C CNN
F 2 "Molex_SlimStack:SlimStack_0.8mm_Male_10" H 4550 2550 50  0001 C CNN
F 3 "~" H 4550 2550 50  0001 C CNN
	1    4550 2550
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J2
U 1 1 6070D476
P 4550 3950
F 0 "J2" H 4600 4367 50  0000 C CNN
F 1 "To bottom plate" H 4600 4276 50  0000 C CNN
F 2 "Molex_SlimStack:SlimStack_0.8mm_Male_10" H 4550 3950 50  0001 C CNN
F 3 "~" H 4550 3950 50  0001 C CNN
	1    4550 3950
	1    0    0    -1  
$EndComp
$Comp
L VL53L5:VL53L5 U1
U 1 1 6086980F
P 3950 7650
F 0 "U1" H 5250 8037 60  0000 C CNN
F 1 "VL53L5" H 5250 7931 60  0000 C CNN
F 2 "millibot-sensors:VL53L5" H 5250 7890 60  0001 C CNN
F 3 "" H 3950 7650 60  0000 C CNN
	1    3950 7650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 6086CE3A
P 2300 8100
F 0 "C1" H 2415 8146 50  0000 L CNN
F 1 "100nF" H 2415 8055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2338 7950 50  0001 C CNN
F 3 "~" H 2300 8100 50  0001 C CNN
	1    2300 8100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 6086DCE2
P 2900 8500
F 0 "C2" H 3015 8546 50  0000 L CNN
F 1 "4.7uF" H 3015 8455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2938 8350 50  0001 C CNN
F 3 "~" H 2900 8500 50  0001 C CNN
	1    2900 8500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 6086F863
P 2900 8650
F 0 "#PWR03" H 2900 8400 50  0001 C CNN
F 1 "GND" H 2905 8477 50  0000 C CNN
F 2 "" H 2900 8650 50  0001 C CNN
F 3 "" H 2900 8650 50  0001 C CNN
	1    2900 8650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 6087025F
P 2300 8250
F 0 "#PWR01" H 2300 8000 50  0001 C CNN
F 1 "GND" H 2305 8077 50  0000 C CNN
F 2 "" H 2300 8250 50  0001 C CNN
F 3 "" H 2300 8250 50  0001 C CNN
	1    2300 8250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 60870CD2
P 5300 8950
F 0 "#PWR04" H 5300 8700 50  0001 C CNN
F 1 "GND" H 5305 8777 50  0000 C CNN
F 2 "" H 5300 8950 50  0001 C CNN
F 3 "" H 5300 8950 50  0001 C CNN
	1    5300 8950
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR02
U 1 1 60872A64
P 2600 7600
F 0 "#PWR02" H 2600 7450 50  0001 C CNN
F 1 "+3V3" H 2615 7773 50  0000 C CNN
F 2 "" H 2600 7600 50  0001 C CNN
F 3 "" H 2600 7600 50  0001 C CNN
	1    2600 7600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 8150 3850 8150
Wire Wire Line
	3850 8150 3850 8250
Wire Wire Line
	3850 8250 3950 8250
Wire Wire Line
	5300 8950 5300 8850
Wire Wire Line
	5300 8850 3850 8850
Wire Wire Line
	3850 8850 3850 8250
Connection ~ 3850 8250
Connection ~ 5300 8850
Wire Wire Line
	3950 8350 2900 8350
Connection ~ 2900 8350
Wire Wire Line
	2300 7950 3950 7950
Wire Wire Line
	2600 7600 2600 7650
Wire Wire Line
	2600 7650 2300 7650
Connection ~ 2300 7950
Connection ~ 2600 7650
Wire Wire Line
	2600 7650 2900 7650
Wire Wire Line
	2900 7650 2900 8350
Connection ~ 2900 7650
Wire Wire Line
	2900 7650 3950 7650
Wire Wire Line
	2300 7650 2300 7950
Text GLabel 7000 8050 2    50   Input ~ 0
MOSI
Wire Wire Line
	7000 8050 6550 8050
Text GLabel 7000 7850 2    50   Output ~ 0
MISO
Wire Wire Line
	7000 7850 6550 7850
Text GLabel 7000 7950 2    50   Input ~ 0
MCLK
Text GLabel 3500 7750 0    50   Input ~ 0
NCS
Text GLabel 3500 8050 0    50   Input ~ 0
LPIN
Text GLabel 3500 7850 0    50   Output ~ 0
GPIO1
Text GLabel 7000 8150 2    50   Input ~ 0
GPIO2
Wire Wire Line
	3500 7750 3950 7750
Wire Wire Line
	3500 7850 3950 7850
Wire Wire Line
	3500 8050 3950 8050
Wire Wire Line
	6550 8150 7000 8150
Wire Wire Line
	6550 7950 7000 7950
Wire Wire Line
	5300 8850 7500 8850
Wire Wire Line
	7500 7650 7500 7750
Wire Wire Line
	6550 7650 7500 7650
Wire Wire Line
	6550 7750 7500 7750
Connection ~ 7500 7750
Wire Wire Line
	7500 7750 7500 8250
Wire Wire Line
	6550 8450 7500 8450
Connection ~ 7500 8450
Wire Wire Line
	7500 8450 7500 8850
Wire Wire Line
	6550 8250 7500 8250
Connection ~ 7500 8250
Wire Wire Line
	7500 8250 7500 8450
Wire Wire Line
	3950 8350 3950 8750
Wire Wire Line
	3950 8750 6700 8750
Wire Wire Line
	6700 8750 6700 8350
Wire Wire Line
	6700 8350 6550 8350
Connection ~ 3950 8350
$EndSCHEMATC
