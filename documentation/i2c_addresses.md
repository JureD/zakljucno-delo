# I2C Addresses of Millibot
## IMU - MPU6050
| AD0 | Address HEX |
|:-- | :-- |
| 0 | 0x68 |
| 1 | 0x69 |

## DC Motor driver - DRV8830
| Contol pin | Value | A3...A0 bits | Address WRITE | Address READ |
| :-- |:-- | :-- | :-- | :-- |
| 0 | 0 | 0000 | 0xC0h  | 0xC1h |
| 0 | open | 0001 | 0xC2h  | 0xC3h |
| 0 | 1 | 0010 | 0xC4h  | 0xC5h |
| open | 0 | 0011 | 0xC6h  | 0xC7h |
| open | open | 0100 | 0xC8h  | 0xC9h |
| open | 1 | 0101 | 0xCAh  | 0xCBh |
| 1 | 0 | 0110 | 0xCCh  | 0xCDh |
| 1 | open | 0111 | 0xCEh  | 0xCFh |
| 1 | 1 | 1111 | 0xD0h  | 0xD1h |

## Distance sensor - VL53L1x
| AD0 | Address HEX |
|:-- | :-- |
| 0 | 0x52 |