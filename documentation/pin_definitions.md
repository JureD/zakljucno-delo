# PIN definitions

## Booting

| PIN | Default | SPI Boot | Download Boot |
| :-- | :-- | :-- | :-- |
| GPIO0 | pull-up | 1 | 0 |
| GPIO46 | pull-down | Don’t-care | 0 |

## Sensors and actuators

PHYSICAL PIN | PIN NAME | Periferna enota | Function | Opomba |
| :-- | :-- | :-- | :-- | :-- |
| 11 | GPIO7 | ADC1_CH6 | Svetlobni senzor 1 |  |
| 12 | GPIO8 | ADC1_CH7 | Svetlobni senzor 2 |  |
| 13 | GPIO9 | ADC1_CH8 | Svetlobni senzor 3 |  |
| 14 | GPIO10 | ADC1_CH9 | Svetlobni senzor 4 |  |
|  |  |  |  |  |
